﻿/*
 * Created by SharpDevelop.
 * User: Rénald
 * Date: 30/07/2016
 * Time: 12:10
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

namespace BusinessModel
{
    public class Product
    {
        public Category Category { get; set; }
        public Command Command { get; set; }
        public string Title { get; set; }
    }
}