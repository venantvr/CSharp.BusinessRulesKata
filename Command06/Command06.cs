﻿/*
 * Created by SharpDevelop.
 * User: Rénald
 * Date: 30/07/2016
 * Time: 12:10
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using BusinessModel;
using Contracts;

namespace BusinessRules
{
    [Export(typeof (IBusinessRule))]
    [Description("Si le paiement est pour la vidéo 'apprendre à skier', ajouter une vidéo gratuite 'premier secours' au bordereau d'expédition (le résultat d'une décision de justice en 1997).")]
    public class Command06 : IBusinessRule
    {
        #region IBusinessRule implementation

        public bool Match(Product product)
        {
            return product.Title == "apprendre à skier";
        }

        public Measure Decision
        {
            get { return new Measure(c => Console.WriteLine(@"Ajouter une vidéo gratuite 'premier secours' au bordereau d'expédition (le résultat d'une décision de justice en 1997).")); }
        }

        #endregion
    }
}