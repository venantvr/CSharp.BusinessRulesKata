using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestProject
{
    [TestClass]
    public class Test_Mef_6_Rules
    {
        [TestInitialize]
        public void Prepare()
        {
            foreach (var file in Directory.EnumerateFiles(AppDomain.CurrentDomain.BaseDirectory, @"Command03.dll"))
            {
                File.Move(file, file + @".old");
            }
        }

        [TestMethod]
        public void Test_For_6_Rules()
        {
            var bre = new BusinessRulesEngine.BusinessRulesEngine(null);

            Assert.IsTrue(bre.Rules.Count == 6);
        }

        [TestCleanup]
        public void CleanUp()
        {
            foreach (var file in Directory.EnumerateFiles(AppDomain.CurrentDomain.BaseDirectory, @"Command03.dll.old"))
            {
                File.Move(file, file.Replace(@".old", string.Empty));
            }
        }
    }
}