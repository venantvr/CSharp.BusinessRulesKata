using System.Collections.Generic;
using System.IO;
using System.Text;

namespace TestProject
{
    public class ConsoleWriter : TextWriter
    {
        public List<string> Lines { get; } = new List<string>();
        public override Encoding Encoding => Encoding.UTF8;

        public override void WriteLine(string value)
        {
            Lines.Add(value);
        }
    }
}