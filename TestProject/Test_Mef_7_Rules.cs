﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestProject
{
    [TestClass]
    public class Test_Mef_7_Rules
    {
        [TestInitialize]
        public void Prepare()
        {
        }

        [TestMethod]
        public void Test_For_7_Rules()
        {
            var bre = new BusinessRulesEngine.BusinessRulesEngine(null);

            Assert.IsTrue(bre.Rules.Count == 7);
        }

        [TestCleanup]
        public void CleanUp()
        {
        }
    }
}