﻿/*
 * Created by SharpDevelop.
 * User: Rénald
 * Date: 30/07/2016
 * Time: 12:10
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using BusinessModel;
using Contracts;

namespace BusinessRules
{
    [Export(typeof (IBusinessRule))]
    [Description("Si le paiement est pour une mise à niveau vers une adhésion, appliquer la mise à niveau.")]
    public class Command04 : IBusinessRule
    {
        #region IBusinessRule implementation

        public bool Match(Product product)
        {
            return product.Category == Category.Upgrade;
        }

        public Measure Decision
        {
            get { return new Measure(c => Console.WriteLine(@"Appliquer la mise à niveau.")); }
        }

        #endregion
    }
}